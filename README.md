# MICROCONTROLLER - Arduino
## Gluttonous Snake                                                                     

>Dec. 2015 
>Department of Physics, Fudan University                                                
>Supervisor: Prof. Xi Yu

* Designed and assembled a Gluttonous Snake game with Arduino UNO and LED dot matrix.
* Find a solution to equip it with a real-time infrared remote controller.

