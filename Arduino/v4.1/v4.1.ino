//version in two control panel
//#include "InfraredRemote.h"

#define RowA 2    //行信号,驱动138 
#define RowB 3
#define RowC 4
#define RowD 5

#define L A2
#define R A3
#define U A4
#define D A5
#define START A1

#define X 7
#define Y 8
#define Z 9

int OE=6; //138 使能

//使用了硬件SPI，以下脚不能更改
#define CLK 13         //时钟    SCK
#define LATCH 10         //595 刷新显示  SS

int command_mark=0;
int matrix[][16] = {
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};
int matrix_gameover[][16]={
0,1,1,0,0,1,1,0,1,0,0,1,1,1,1,1,
1,0,0,1,1,0,0,1,1,1,1,1,1,1,1,0,
1,0,1,1,1,1,1,1,1,0,0,1,1,0,0,0,
0,1,1,1,1,0,0,1,1,0,0,1,1,1,1,1,
0,1,1,0,1,0,0,1,1,1,1,1,0,1,1,0,
1,0,0,1,1,0,0,1,1,1,1,0,1,0,0,1,
1,0,0,1,0,1,1,0,1,0,0,0,1,1,1,0,
0,1,1,0,0,1,1,0,1,1,1,1,1,0,0,1,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
};
int signal[][16]={
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};
int tail[2]={15,0};
int head[2]={15,3};
int snake[256][2];
int length=4;
int ahead[2]={15,4};
int direct=2;
int food[2]={15,0};
int tag=0;
int k=0;

void init_snake(){
  tail[0]=15;tail[1]=0;
  head[0]=15;head[1]=3;
  snake[0][0]=head[0];snake[0][1]=head[1];
  snake[1][0]=15;snake[1][1]=2;snake[2][0]=15;snake[2][1]=1;snake[3][0]=tail[0];snake[3][1]=tail[1];
  length=4;
  ahead[0]=15;ahead[1]=4;
  direct=2;
  food[0]=0;food[1]=0;
  tag=0;
  for(int i=0;i<16;i++){
    for(int j=0;j<16;j++){
      matrix[i][j]=0;
    }
  }
  for(int i=0;i<4;i++){
    matrix[15][i]=1;
  }
};

void setfood(){
  int a,b;
  do{a=random(0,15);
     b=random(0,15);
  }while(matrix[a][b]==1);
  matrix[a][b]=1;
  food[0]=a;food[1]=b;
};

void check(){
  switch(direct){
    case 1:
      {ahead[0]=head[0]+1;
      ahead[1]=head[1];}
      break;
    case 2:
      ahead[0]=head[0];
      ahead[1]=head[1]+1; 
      break; 
    case 3:
      ahead[0]=head[0]-1;
      ahead[1]=head[1];
      break;
    case 4:
      ahead[0]=head[0];
      ahead[1]=head[1]-1;
      break;  
  }
  if(ahead[0]>15||ahead[0]<0||ahead[1]>15||ahead[1]<0){
    tag=1;
  }else if(matrix[ahead[0]][ahead[1]]==1&&(ahead[0]!=food[0]||ahead[1]!=food[1])){
    tag=1;
  };
};

void goAhead(){
  if(tag==1){gameover();}
  else{
    head[0]=ahead[0];
    head[1]=ahead[1];
    matrix[head[0]][head[1]]=1;
    for(int i=length;i>0;i--){
      snake[i][0]=snake[i-1][0];
      snake[i][1]=snake[i-1][1];
    }
    snake[0][0]=ahead[0];
    snake[0][1]=ahead[1];
    if(ahead[0]!=food[0]||ahead[1]!=food[1]){
      matrix[tail[0]][tail[1]]=0;
      tail[0]=snake[length-1][0]; 
      tail[1]=snake[length-1][1];
    }else{
      length++;
      setfood();
    }
    getSignal(matrix,signal);
  };
};

/*void gameover(){
  for(int i=0;i<16;i++){
    for(int j=0;j<16;j++){
      matrix[i][j]=0;
    }
    matrix[i][i]=1;
    matrix[i][15-i]=1;
  }
};*/
void gameover(){
  /*for(int i=0;i<16;i++){
    for(int j=0;j<16;j++){
      matrix[i][j]=0;
    }
    matrix[i][i]=1;
    matrix[i][15-i]=1;
  }*/
  delay(500);
};



int power(int a,int b){
  int c=1;
 for(int i=0;i<b;i++){
  c*=a;
 } 
 return c;
}
void getSignal(int matrix[][16],int signal[][16]){
  for(int i=0;i<16;i++){
      signal[0][i]=0;
      for(int j=0;j<8;j++){
        signal[0][i]+=power(2,(7-j))*matrix[i][j];
      }
      signal[1][i]=0;
      for(int j=8;j<16;j++){
        signal[1][i]+=power(2,(15-j))*matrix[i][j];
      }
  }
}

void spi_transfer(volatile char data)//硬件SPI
{
  SPDR = data;                    // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
  //return SPDR;                    // return the received byte
}

void hc138sacn(byte r){  //输出行线状态ABCD （A低,D高)
    digitalWrite(RowA,(r & 0x01));
    digitalWrite(RowB,(r & 0x02));
    digitalWrite(RowC,(r & 0x04));
    digitalWrite(RowD,(r & 0x08));
}

void show(){
  for(int row=0;row<16;row++){
    for (int i=0;i<2;i++){      
      spi_transfer(~(signal[i][row]));  
    }
//    digitalWrite(OE, 1);  //关闭显示
    hc138sacn(15-row);            //换行
    digitalWrite(LATCH, 0);      //595刷新       
    digitalWrite(LATCH, 1);
    delayMicroseconds(500) ;   //节电用,
    digitalWrite(OE, 0);  //开启显示
    delayMicroseconds(500) ;
  }
}

void getCommand2(){
  int mark=0;
  if(digitalRead(7)==HIGH){mark+=4;}
  if(digitalRead(8)==HIGH){mark+=2;}
  if(digitalRead(9)==HIGH){mark+=1;}
  
  if(mark==4&&direct!=2){direct=4;}
  else if(mark==5&&direct!=4){direct=2;}
  else if(mark==7&&direct!=3){direct=1;}
  else if(mark==6&&direct!=1){direct=3;}
  else if(mark==0&&command_mark!=0||mark==1&&command_mark!=1){
    init_snake();
    setfood();//open
    tag=1;
  }
  else if(mark==2&&command_mark!=2||mark==3&&command_mark!=3){
    if(tag){tag=0;}else{tag=1;}
  }  
  command_mark=mark;
}

void setup () {

    pinMode(RowA, OUTPUT);
    pinMode(RowB, OUTPUT);
    pinMode(RowC, OUTPUT);
    pinMode(RowD, OUTPUT); //138片选
    pinMode(OE, OUTPUT); //138 使能
  
    pinMode(12, OUTPUT);//595 数据
    digitalWrite(12,HIGH);
    pinMode(11,OUTPUT);
    pinMode(CLK, OUTPUT); //595 时钟
    pinMode(LATCH, OUTPUT); //595 使能

    pinMode(7,INPUT);
    pinMode(8,INPUT);
    pinMode(9,INPUT);
    
    Serial.begin(9600);
 
    SPCR = (1<<SPE)|(1<<MSTR);
    delay(10);
    init_snake();
    setfood();
    tag=1;
    getSignal(matrix,signal);
    
}

void loop(){ 

  int time=20;
  getSignal(matrix,signal);
  for(int t=0;t<time;t++){
    show();
  }  

  getCommand2();

  if(tag==0){
    check();
    goAhead();
  }  
  
}



